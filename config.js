// Environment Vars
var appPort = process.env.APPPORT || 3102;

var config = {
    server: {
		host: "0.0.0.0",
		port: appPort
	},

	db: {
		host: "localhost",
		port: 27017, 
		options: { auto_reconnect: true }
	}
};

module.exports = config;