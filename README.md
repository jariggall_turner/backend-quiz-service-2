# backend-quiz-service-2 - Express/MongoDB Web Service #
Web Service that receives JSON posts, stores JSON document in MongoDB, and has methods to retrive all documents and as well as documents by id.  

### Dependencies ###
Implemented in Node.js v.0.10.*  
Express v4.11.2  
MongoDB v1.4.2  

### Source Descriptions ###
service.js - express server and route mappings  
routes.js - route actions, MongoDB logic  
config.js - environment settings, feed locations  

### Routes ###
**GET** /healthcheck  
Description: service status  
Example: http://cn-games-services.turner.com:3102/healthcheck  

**GET** /docs  
Description: retrieve all stored documents  
Example: http://cn-games-services.turner.com:3102/docs 

**POST** /docs  
Description: add new JSON document  
Example: http://cn-games-services.turner.com:3102/docs  
JSON Body:  
{  
    "id": 3,  
    "name": "Marty McFly",  
    "hobbies": "skateboarding, time travel",  
    "mentor": "Doctor Emmett Brown"  
}  

Curl Example: curl -H "Content-Type: application/json" -d '{"id": 6,"name": "George McFly","hobbies": "Sc-Fi","mentor": "Jules Verne"}' http://cn-games-services.turner.com:3102/docs  

**GET** /docs/:doc_id  
Description: get document by MongoDB ObjectID (_id)  
Example: http://cn-games-services.turner.com:3102/docs/54d951b0e517fc5b77b24fdb  

**PUT** /docs/:doc_id  
Description: update JSON document by MongoDB ObjectID (_id)  
Example: http://cn-games-services.turner.com:3102/docs/54d951b0e517fc5b77b24fdb  
JSON Body:  
{  
    "name": "Marty McFly",  
    "hobbies": "hover boards, time travel",  
    "mentor": "Doctor Emmett Brown"  
}