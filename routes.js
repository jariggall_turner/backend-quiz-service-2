var mongo = require("mongodb"),
	config = require("./config");

// initialize db and collection
var server = new mongo.Server(config.db.host, config.db.port, config.db.options),
	db = new mongo.Db("docsdb", server),
	collection = db.collection("docs");

// open db
db.open(function (err, db) {
	if (err) {
		console.error("[error] failed to connect to MongoDB");
	} else {
		db.collection("docs", {string:true}, function (err, collection) {
			if (err) {
				console.error("[error] the 'docs' collection doesn't exist. Insert a doc first.");
			}
		});
	}
});

// healthcheck
exports.healthcheck = function (req, res) {
	res.send({ status: "ok", uptime: process.uptime() });
};

// insert doc
exports.addDoc = function (req, res) {
	var doc = req.body;
	console.log("[info] Adding doc: " + JSON.stringify(doc));

	collection.insert(doc, {safe:true}, function (err, result) {
		if (err) {
			console.error("[error] - %s", err);
			res.send({"error": err});
		} else {
			res.send(result[0]);
		}
	});
};

// find all
exports.findAllDocs = function (req, res) {
	collection.find().toArray(function (err, items) {
		if (err) {
			console.error("[error] - %s", err);
			res.send({"error": err});
		} else {
			res.send(items);
		}
	});
};

// find by id
exports.findDocByID = function (req, res) {
	var id = req.params.doc_id;
	console.log("[info] Retrieving doc: " + id);	
	
	collection.findOne({"_id": new mongo.BSONPure.ObjectID(id)}, function (err, item) {
		if (err) {
			console.error("[error] - %s", err);
			res.send({"error": err});
		} else {
			res.send(item);
		}
	});
};

// update doc
exports.updateDoc = function (req, res) {
	var id = req.params.doc_id,
		doc = req.body;
	
	console.log("[info] Updating doc: " + id);

	collection.update({"_id": new mongo.BSONPure.ObjectID(id)}, doc, {safe:true}, function (err, result) {
		if (err) {
			console.error("[error] - %s", err);
			res.send({"error": err});
		} else {
			res.send(doc);
		}
	});
};