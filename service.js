var express = require("express"),
	bodyParser = require("body-parser"),
	routes = require("./routes"),
	config = require("./config");

var app = express();

// app settings
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// logger
app.use(function(req, res, next) {
	console.log('%s %s %s', req.method, req.url, req.path);
	next();
});

// routes
app.get("/healthcheck", function (req, res) {
	routes.healthcheck(req, res);
});

app.post("/docs", function (req, res) {
	routes.addDoc(req, res);
});

app.get("/docs", function (req, res) {
	routes.findAllDocs(req, res);
});

app.get("/docs/:doc_id", function (req, res) {
	routes.findDocByID(req, res);
});

app.put("/docs/:doc_id", function (req, res) {
	routes.updateDoc(req, res);
});

app.get("*", function (req, res) {
	res.json({"app": "docs"});
});

// start express server
var server = app.listen(config.server.port, function () {
	console.log("Express listening at http://%s:%s", server.address().address, server.address().port);
});
